# json-sane

Sane JSON library that prints a more sane size JSON output elided if necessary, especially where arrays are concerned.  And also handles circular JSON properly.

# Usage

```javascript
const replacer = require('json-sane');
console.log(JSON.stringify(obj, replacer(), 2));
```
Note the `replacer` is a function that returns a function, so you must include the `()` where the replacer is used.
