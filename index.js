'use strict';

/*
 * This module guards against two problems in JSON stringification:
 *   1) circular references
 *   2) run-away text dumps that need to be elided
 */

const saneReplacer = () => {
  const priors = {}; // prior keys that referenced the same value
                     // that is now flagging a circular reference
  const seen = new WeakSet();
  let rootValue;

  return (key, value) => {

    // Avoid Circular References
    if (typeof value === "object" && value !== null) {
      if (!rootValue) rootValue = value;
      if (seen.has(value)) {
        priors[value] = priors[value].push(key);
        if (value === rootValue)
          return "[[[ CIRCULAR-REFERENCE(ROOT) ]]]";
        else {
          return "!!!!! CIRCULAR-REFERENCE-PRIORS [" + priors[value] + "] !!!!!";
        }
      } else {
        seen.add(value);
        priors[value] = [key];
      }

      // Elide extra long text generation
      if (key != undefined && key != null && key.length > 0 &&
         value != undefined && value != null) {
        let objChildren = false;
        let elided = false;

        if (Array.isArray(value)) {
          if (value.length > 100) {
            value = value.slice(0, 10);
            value = JSON.stringify(value, saneReplacer());
            value = value.substring(0, Math.min(60, value.length - 1)).concat(',...');
            elided = true;
          } else {
            for (let i = 0; i < Math.min(10, value.length); i++) {
              let child = value[i];
              if (typeof child === 'object') {
                objChildren = true;
                break;
              }
            }
            if (objChildren == false) {
              if (value.length > 10)
                value = value.slice(0, 10);
              value = JSON.stringify(value, saneReplacer());
              if (value.length > 80)
                value = value.substring(0, Math.min(80, value.length - 1)).concat(',...');
              elided = true;
            }
          }
        } else if (typeof value === 'object') {
          let keys = Object.keys(value);
          for (let i = 0; i < Math.min(10, keys.length); i++) {
            let childKey = keys[i];
            let child = value[childKey];
            if (typeof child === 'object') {
              objChildren = true;
              break;
            }
          }

          if (objChildren === false && keys.length < 5) {
            value = JSON.stringify(value, saneReplacer());
            value = value.replace(/\"/g, "\'").replace(/:/g, ": ");
            elided = true;
          }
        }
        if (elided == false && typeof value === "string") {
          if (value.length > 80)
            value = value.substring(0, Math.min(80, value.length - 1)).concat('...');
        }
      }
    }
    return value;
  }
}

module.exports = saneReplacer;
