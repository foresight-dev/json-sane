'use strict';

const replacer = require('./index');

class NormalCase {
   constructor() {
      this.a = '1';
      this.b = '2';
      this.c = '3';
   }
}

class CircularCase {
   constructor() {
      this.a = '1';
      this.b = '2';
      this.c = this;
   }
}

class VerboseCase {
   constructor() {
      this.a = '1';
      this.b = '2';
      this.c = '3';
      this.d = [];
      for(let i=0; i<1000; i++) this.d.push(i);
   }
}

class VerboseCircularCase {
   constructor() {
      this.a = '1';
      this.b = '2';
      this.c = this;
      this.d = [];
      for(let i=0; i<1000; i++) this.d.push(i);
   }
}


test('stringify normal object', () => {
   let output = JSON.stringify(new NormalCase(), replacer());
   expect(output).toMatch(/\"c\"\:\"3\"/);
});

test('stringify circular referencing object', () => {
   let output = JSON.stringify(new CircularCase(), replacer());
});

test('stringify verbose object', () => {
   let output = JSON.stringify(new VerboseCase(), replacer());
   expect(output).toMatch(/9,\.\.\./);
});

test('stringify verbose circular object', () => {
   let output = JSON.stringify(new VerboseCircularCase(), replacer());
   expect(output).toMatch(/CIRCULAR-REFERENCE.*9,\.\.\./);
});
